"Le Temple nâga" by Natrium729

[
Pour faire bref :
Ce code source est libre ; vous pouvez le redistribuer ou le modifier suivant les termes de la « GNU General Public License » telle que publiée par la Free Software Foundation : soit la version 3 de cette licence, soit toute version ultérieure.

Ce programme est distribué dans l'espoir qu'il vous sera utile, mais SANS AUCUNE GARANTIE : sans même la garantie implicite de COMMERCIALISABILITÉ ni d'ADÉQUATION À UN OBJECTIF PARTICULIER. Consultez la Licence Générale Publique GNU pour plus de détails.

Pour faire long, consultez : http://www.gnu.org/licenses
]

Volume 1 - Initialisation

Book 1 - Extensions

Part 1 - French

Include French by Eric Forgeot. Use French 1PSPr Language.

Part 2 - Basic Screen Effects

Include Basic Screen Effects by Emily Short.

Book 2 - Options

Use full-length room descriptions.
Use no scoring.

Book 3 - Status Bar

Table of Status Bar
left	central	right
"T:[turn count]"	"[the player's surroundings]"	"Hydratation :"
""	""	"[hydratation]"

Rule for constructing the status line:
	fill status bar with Table of Status Bar;
	rule succeeds.

Book 4 - Règles

Part 1 - Obscurité

Instead of going from a dark room to an not visited room, say "Je ne verrai pas par où je passe dans ce noir d'encre.".

Book 5 - Joueur

Part 1 - Hydratation

Chapter 1 - Hydratation

The player has a number called hydratation. The hydratation of the player is 100.

To say hydratation:
	if hydratation of the player <= 30, turn the background red;
	say "[hydratation of the player] %";
	turn the background white.

Chapter 2 - Heat

Every room has a number called heat. The heat of a room is usually ten.

Every turn:
	decrease the hydratation of the player by the heat of the location;
	if hydratation of the player <= 0
	begin;
		say "Je ne sens plus mon corps sous le poids écrasant de la chaleur. Mes os blanchiront à jamais ici, et le monde sombrera dans le chaos…";
		wait for any key;
		end the story saying "C'est la fin !";
	end if.

After looking when the heat of the location is 0, now the hydratation of the player is 100.

Before printing the name of a room (called R) while not constructing the status line:
	if heat of R >= 7, say red letters;
	if heat of R <= 6, say yellow letters;
	if heat of R <= 3, say green letters;
	if heat of R is 0, say blue letters.
	
After printing the name of a room while not constructing the status line, say default letters.

Before printing the name of a room (called R) while constructing the status line:
	if heat of R >= 7, turn the background red;
	if heat of R <= 6, turn the background yellow;
	if heat of R <= 3, turn the background green;
	if heat of R is 0, turn the background blue.

After printing the name of a room while constructing the status line, turn the background white.

Chapter 3 - Informations

After printing the banner text:
	say "[first time]Tapez « aide » pour obtenir plus d'informations sur le jeu ou si vous jouez à une fiction interactive pour la première fois.[paragraph break][italic type]Note : l'hydratation du héros (notée en haut à droite) correspond à l'eau qu'il reste à son corps. Sa valeur maximale est 100.[line break]Chaque tour, elle baisse en fonction de la chaleur de l'endroit. La couleur dont est écrit le nom du lieu permet de s'en faire une idée : rouge pour un lieu chaud, jaune pour un lieu à température moyenne et vert pour un lieu relativement froid.[line break]Le bleu indique quant à lui que l'endroit contient de l'eau et l'hydratation remontera à 100. Le héros meurt si elle atteint 0.[roman type][line break][only]";

Volume 2 - Jeu

Book 1 - Introduction

When play begins:
	say "[italic type]Le fond de l'océan avait tremblé cette nuit-là. Effrayés par ce phénomène encore nouveau pour nous, nous crûmes à la fin du monde.";
	wait for any key;
	say "Nous nous réunîmes alors devant le temple sacré de notre Déesse, où la prêtresse tint ces mots :";
	wait for any key;
	say "[paragraph break]« La prophétie est sur le point de se réaliser, et le démon enfoui au plus profond des entrailles de la Terre se réveille. » déclara-t-elle gravement.";
	wait for any key;
	say "[line break]Nous restâmes muets de terreur ; on aurait pu entendre une méduse nager. Mais elle continua :";
	wait for any key;
	say "[paragraph break]« Cependant, il reste un espoir. Le plus jeune d'entre nous ayant déjà fini ses neuf mues devra se rendre dès maintenant à la surface, sur l[']île la plus proche. En ces lieux, il devra trouver l'Autel de la Lumière. Là lui sera révélée la solution. »";
	wait for any key;
	say "[paragraph break]Il y eut des murmures dans l'assemblée. Le sauveur du peuple Nâga choisi par la prophétie, c[']était moi. Sous les prières alors murmurées, je nageai vers la surface.";
	wait for any key;
	say "[line break]Encore et toujours, sans m'arrêter.[roman type][line break]";
	wait for any key;
	now the description of the player is "Je suis un jeune Nâga ordinaire, qui viens de finir mes neuf mues et mes écailles bleues indiquent maintenant que je suis un adulte. Choisi par la prophétie, je dois maintenant m'aventurer hors de tout ce que je connaissais pour sauver mon peuple. Je n'ai pas de temps à perdre.".

Book 2 - Jeu

Part 1 - Océan

The océan is a room. "Le soleil s'est levé il y a quelques heures sur cet océan à perte de vue. La mer est calme, comme si elle avait déjà oublié les événements de la nuit dernière[unless the player carries the orbe]. Ma patrie se trouve sous moi, mais il me faut continuer : je vois la ligne d'une plage au nord[end unless]."
The printed name is "Parmi les vagues".
The heat is 0.

Instead of going nowhere from the océan:
	if incendie is happening, say "Je dois plonger ! Vite !";
	else say "Non, je dois atteindre l[']île.".

Instead of downgoing in océan, try swimming.

Instead of jumping when the location is océan or the location is lagon, say "Je suis en train de nager.".

Instead of dropping something when the location is océan or the location is lagon, say "Si je pose [the noun] ici, je ne pourrai plus [if the noun is female]la[else]le[end if] récupérer.". 

Instead of swimming in océan when the player carries the orbe:
	say "Sans perdre un instant, je plonge, l'Orbe dans mes mains. Je nage du mieux que je peux, mais la fatigue me rattrape. Non, je dois réussir ! Vite, il faut que j'arrive à temps !";
	wait for any key;
	end the story finally saying "À suivre…".

Instead of swimming when the location is océan or the location is lagon or the location is lac, say "Je suis déjà en train de nager.".

Chapter 1 - Eau

The eau is backdrop. The eau is in océan, lagon, falaises, falaisen, plage, plageo and plagee.
The printed name is "océan".
Understand "ocean" or "mer" as the eau.
The description is "Une étendue d'eau salée rejoignant l'horizon.".

Instead of outgoing the eau when the location is océan or the location is lagon, try going north.

Instead of taking the eau:
	if the player carries the galet or the player carries the pelle, say "J'ai de quoi la transporter, mais il sera difficile de me déplacer ensuite.";
	else say "Je n'ai rien pour la transporter.".

Instead of entering the eau:
	if the location is falaises or the location is falaisen or the location is plageo
	begin;
		try jumping;
	else if the location is plage or the location is plagee;
		try going south;
	else;
		try swimming;
	end if.

Instead of tasting the eau, say "Salé.".

Before eating the eau:
	say "Cela ne se mange pas !";
	stop the action.

Instead of drinking the eau:
	if the location is falaises or the location is falaisen or the location is plageo
	begin;
		try jumping;
	else if the location is plage or the location is plagee;
		try going south;
	else;
		say "Être dans mon élément me suffit.";
	end if.

Instead of inserting something into the eau, say "Il valait mieux garder [the noun] avec moi.".

Instead of pushing or pulling the eau, say "Je ne peux pas faire cela !".

Part 2 - Lagon

The lagon is south from the plagee. "Un magnifique lagon turquoise, où il fait bon nager. La barrière de corail m'empêche de rejoindre l'océan, mais de toute façon, je ne dois pas me détourner de mon objectif ; je ferais mieux de revenir sur la terre ferme au nord.".
The printed name is "Dans l'eau du lagon".
The heat is 0.

Instead of going down when the location is lagon, say "Je plonge un instant, mais revient vite à la surface : je ne dois pas perdre de temps.".

Chapter 1 - Barrière de corail

The corail is female scenery in lagon.
The printed name is "barrière de corail".
Understand "barriere" or "barriere de corail" as the corail.
The description is "Un immense mur sous-marin fait de coraux. D'ici, il m'empêche de rejoindre l'océan.".

Part 3 - Plage

The plage is north from the océan. "Une longue étendue de sable blanc se poursuivant à l'est et à l'ouest. La chaleur est étouffante[unless the player carries the orbe]. Je devrais rejoindre l'abri que m'offrent les arbres au nord ; en effet, la jungle succède directement à cette plage ondulée[end unless]."
The printed name is "Sur le sable".

Instead of swimming when the location is plage, try going south.

Chapter 1 - Sable

The sable is a backdrop. The sable is in plage, plageo and plagee.
The description is "Un sable blanc et brûlant. Je ne devrais pas rester ici.".

Instead of taking or touching the sable, say "Jamais je ne mettrai mes mains dans ce sable brûlant !".

Instead of tasting the sable, say "Jamais !"

Understand "creuser [thing]" or "creuser" as searching.

Rule for supplying a missing noun while searching when the sable is touchable:
	now the noun is the sable.

Instead of searching the sable:
	If the player carries the galet
	begin;
		say "Le galet pourrait m'aider, mais je n'ai rien pour le tenir pendant que je creuse : il me faudrait un manche.";
	else if the player does not carry the pelle;
		say "Je n'ai rien qui me permettrait de creuser. Et jamais je ne mettrai mes mains dans ce sable brûlant !";
	else if the location is plagee and the coffret is off-stage;
		say "Sous la torride chaleur du soleil, je creuse, transpirant à grosses gouttes. Enfin, ma pelle bute contre quelque chose de dur. Un coffret !";
		decrease the hydratation of the player by 20;
		say "[line break][italic type] L'hydratation du personnage a baissé de 20 %.[roman type][line break]";
		now the coffret is in the plagee;
	else;
		say "Je ne trouve rien d'intéressant.";
	end if.
	
Chapter 2 - Coffret

The coffret is a closed openable container. "Le petit coffret que j'ai déterré gît près du trou d'où il vient.".
The description is "Un vieux coffret, un peu abîmé par le temps. Son loquet est tout rouillé et je n'aurai sûrement aucun mal à l'ouvrir."

Chapter 3 - Clef rouillée

The clef unlocks the trappe. The clef is female. The clef is in the coffret.
The printed name is "clef rouillée".
Understand "cle" or "clef/cle rouillee" as the clef.
The description is "Une clef antique, rouillée par le temps. Je devrais relire le parchemin, peut-être apprendrai-je ce qu'elle ouvre ?".

Part 4 - Plage ouest

The plageo is west from the plage. "La plage laisse rapidement place à des rochers où viennent se briser les vagues. Ici, impossible de plonger : ces pierres glissantes et tranchantes rendraient le retour à la terre ferme impossible. Je ne peux que retourner sur la plage à l'est, aller plus loin sur la côte au nord-ouest ou m'aventurer dans la jungle au nord.".
The printed name is "Surplombant la mer".
The heat is 8.

Instead of jumping when the location is plageo:
	say "Les rocher sont trop nombreux, et la houle est trop forte.".

Instead of swimming when the location is plageo, try jumping.

Chapter 1 - Galet

The galet is a thing in the plageo. "Un étrange galet attire mon attention.".
Understand "pierre" or "caillou" as the galet.
The description is "Une étrange pierre creuse. On aurait presque dit un bol. Je me demande ce qui a pu lui donner cette forme.".

Instead of tying the galet to something, say "Je devrais attacher le galet à autre chose.".
Instead of tying something to the galet, say "Je devrais attacher le galet à autre chose.".

Chapter 2 - Pierres

Some rocs are scenery in the plageo.
Understand "rochers" or "pierres" as the rocs.
The description is "Des rochers glissants et acérés brisent les vagues.".

Part 5 - Plage est

The plagee is east from the plage and southeast from the forêt. "De ce côté, le sable brille plus fort que jamais. Le soleil est écrasant. Je ne devrais pas m'attarder en ce lieu. Heureusement, l'océan n'est pas loin[unless the player carries the orbe], et le couvert des branches m'attend au nord-ouest[end unless].".
The printed name is "Sur le sable".
The heat is 11.

Instead of swimming when the location is plagee, try going south.

Part 6 - Falaise sud

The falaises is northwest from the plageo, west from the ruines and southwest from the forêt2. "Une large falaise s[']étirant plus au nord, et dominant l'océan de toute sa hauteur. Il serait suicidaire de sauter ; je devrais rentrer plus dans les terres à l'est.".
The printed name is "Loin au-dessus de l'eau".
The heat is 7.

Instead of jumping when the location is falaises or the location is falaisen:
	say "Succombant à l'appel de l'océan, je saute, plonge et me fracasse contre le roc.";
	wait for any key;
	end the story saying "C'est la fin !".

Instead of swimming when the location is falaises or the location is falaisen, try jumping.

Part 7 - Falaise nord

The falaisen is north from the falaises, northwest from the ruines and west from the forêt2. "Une haute falaise continuant plus au sud. En contrebas, une mâchoire de rocs me broierait si je sautais[unless the player carries the orbe]. Je ferais mieux de retourner dans la forêt à l'ouest[end unless].".
The printed name is "Loin au-dessus de l'eau".
The heat is 6.

Part 8 - Ruines

The ruines is north from the plageo, west from the forêt and northwest from the plage. "D[']étranges restes de bâtiments, abandonnés à la nature qui a repris ses droits. Bizarrement, les décombres me rappellent l'architecture de nos cités.[line break]Seule, une construction de pierre semble tenir tête à la forêt tout entière. Je peux m'enfoncer dans la forêt au nord et à l'est, j'entends le son de l'eau au nord-est et la jungle s[']éclaircit à l'ouest et au sud.".
The printed name is "Dans les ruines".
The heat is 5.

Chapter 1 - Décombres

Some décombres are scenery in the ruines.
Understand "decombres", "gravats" or "restes" as the décombres.
The description is "Des débris moussus, témoins du village qui se dressait ici autrefois. Quelle peut bien être la raison de sa disparition ?".

Chapter 2 - Construction

The construction is female scenery in the ruines.
Understand "batiment" or "maison" as the construction.
The description is "Une étrange bâtisse, faisant front à la nature par sa solidité. Je devrais l'explorer ; elle pourrait contenir un indice.".

Instead of entering the construction, try ingoing.

Part 9 - Pyramide

The pyramide is inside from the ruines. "Une apaisante fraîcheur règne en ce lieu. Le silence ainsi que le poids du temps que dégage cet endroit forcent au respect. Tout ici semble détaché du reste du monde. Peut-être y aura-t-il ici un indice sur l'Autel de la Lumière ?".
The printed name is "À l'intérieur de la pyramide".
The heat is 2.

Chapter 1 - Sarcophage

The sarcophage is a closed openable fixed in place container in the pyramide. "Il y a une sorte de sarcophage ici.".
The description is "Un étrange sarcophage, qui semble ne pas avoir bougé depuis des lustres.[if the sarcophage has not been open] Serait-ce une bonne idée de l'ouvrir ?[end if]".

Chapter 2 - Carte

The carte is a thing in the sarcophage.
The printed name is "lambeau de parchemin".
Understand "lambeau", "lambeau de parchemin" or "parchemin" as the carte.
The description is "Une vieille peau tannée à manipuler avec précaution. Il semblait y avoir quelque chose d[']écrit :[paragraph break]Afin de prot … l'aut … ière des … mons … cacher … lef … sanctuai … ource[paragraph break][carte][paragraph break]On dirait une carte !".

Instead of reading the carte, try examining the carte.

After examining the carte for the first time:
	now the printed name of the carte is "carte";
	now the carte is female.

To say carte:
	say fixed letter spacing;
	say blue letters;
	say "^^^^^^^^^^^^^^^^^^^^^^^^^[line break]^^^^^^^^^^^^^^^^^^^^^^^^^";
	say line break;
	say "^^^[default letters]|MMMMMMMMMMMMMMMMM|[blue letters]^^^";
	say line break;
	say "^^^[default letters]|MMMMMMM[magenta letters] O [default letters]MMMMMMM|[blue letters]^^^";
	say line break;
	say "^^^[default letters]|MMMMMMMMMMMMMMMMM|[blue letters]^^^";
	say line break;
	say "^^^[default letters]|[green letters]&&&&&&&&&&[blue letters]^^^[green letters]&&&[yellow letters] S[blue letters]^^^";
	say line break;
	say "^^^[default letters]|[green letters]&&&&&&&&&&&[blue letters]^^[green letters]&&&[yellow letters] S[blue letters]^^^";
	say line break;
	say "^^^[default letters]|[green letters]&&&[default letters] A [green letters]&&&&&&&&&&[yellow letters] S[blue letters]^^^";
	say line break;
	say "^^^[default letters]|[green letters]&&&&&&&&&&&&&&&&[yellow letters] S[blue letters]^^^";
	say line break;
	say "^^^[default letters]|              [red letters]x[yellow letters]  S[blue letters]^^^";
	say line break;
	say "^^^ [yellow letters]~~~~~~~~~~~~~~~~ [blue letters]^^^^";
	say line break;
	say "^^^^^^^^^^^^^^^^^^^^^^^^^[line break]^^^^^^^^^^^^^^^^^^^^^^^^^";
	say "[variable letter spacing][default letters]".
	
[
^^^^^^^^^^^^^^^^^^^^^^^^^
^^^^^^^^^^^^^^^^^^^^^^^^^
^^^|MMMMMMMMMMMMMMMMM|^^^
^^^|MMMMMMM O MMMMMMM|^^^
^^^|MMMMMMMMMMMMMMMMM|^^^
^^^|&&&&&&&&&&^^^&&& S^^^
^^^|&&&&&&&&&&&^^&&& S^^^
^^^|&&& A &&&&&&&&&& S^^^
^^^|&&&&&&&&&&&&&&&& S^^^
^^^|              x  S^^^
^^^ ~~~~~~~~~~~~~~~~ ^^^^
^^^^^^^^^^^^^^^^^^^^^^^^^
^^^^^^^^^^^^^^^^^^^^^^^^^
]

Part 10 - Forêt

The forêt is north from the plage and northeast from the plageo. "La température est plus supportable ici, mais je ne vois plus où je vais : le feuillage dru cache le soleil et les arbres ne sont pas clairsemés. Cependant, j'entends le clapotis de l'eau plus au nord.".
The printed name is "Entre les arbres".
The heat is 4.

Chapter 1 - Arbres

Some arbres are backdrop. The arbres are in forêt and forêt2.
Understand "arbre", "jungle", "foret", "feuilles" or "feuillage" as the arbres.
The description is "D'immenses troncs serrés, rendant le parcours difficile. Heureusement, les hautes frondaisons me protègent plus ou moins du soleil.".

Instead of climbing the arbres:
	say "Peut-être que de là-haut, j'aurais une meilleure vue de l[']île ? Je commence donc mon ascension.";
	wait for any key;
	say "Sous l'eau, on ne peut rien escalader, alors je ne sais pas trop m'y prendre. Je glisse, et tombe au sol. C'est la fin.";
	wait for any key;
	end the story saying "C'est la fin !".

Chapter 2 - Liane

The liane is a female thing in the forêt. "Une liane traîne par terre.".
The description is "Un morceau de liane à moitié pourrie, qui a certainement dû tomber d'un arbre.".

Instead of tying the liane to something, say "Je devrais plutôt utiliser la liane pour attacher quelque chose !".
Instead of tying something to the liane, say "Je devrais plutôt utiliser la liane pour attacher quelque chose !".

Part 11 - Forêt 2

The forêt2 is north from the ruines, west from the source and northwest from the forêt. "La jungle, toujours aussi dense et impénétrable. Un gargouillis se fait entendre à l'est, mais sinon, rien. Pas le moindre bruit. C'est plutôt inquiétant…".
The printed name is "Entre les arbres".
The heat is 4.

Chapter 1 - Branche

The branche is a female thing in the forêt2. "Une branche est tombée d'un arbre.".
The description is "Une lourde branche, quoique petite. Un coup de vent a dû la détacher.".

Instead of tying the branche to something, say "Je devrais attacher la branche à autre chose.".
Instead of tying something to the branche, say "Je devrais attacher la branche à autre chose.".

Instead of tying the branche to the galet, build a pelle.
Instead of tying the galet to the branche, build a pelle.

To build a pelle:
	if the player carries the liane
	begin;
		remove the liane from play;
		remove the branche from play;
		remove the galet from play;
		now the player carries the pelle;
		say "J'attache la branche au galet grâce à la liane afin de former une sorte de pelle.";
	else;
		say "Je n'ai rien pour les attacher ensemble !";
	end if.
	
Chapter 2 - Pelle

The pelle is a female thing.
The description is "Une pelle rudimentaire, faite d'un galet et d'une branche attachés ensemble par une liane. Je sais, on peut faire mieux comme outil…".


Part 12 - Montagneo

The montagneo is northeast from the falaisen, north from the forêt2 and northwest from the source. "Des contreforts d'où l'on a une magnifique vue sur l'ensemble de l[']île. Impossible sinon de continuer vers le nord : la pente est vraiment trop escarpée. La chaîne continue à l'est et je peux redescendre vers le sud.".
The printed name is "Au pied des montagnes".
The heat is 4.

Chapter 1 - Vue

The vue is backdrop. The vue is in montagneo and montagnee.
Understand "ile" or "paysage" as the vue.
The description is "Jamais je n'ai vu un paysage aussi beau. Sous l'eau, il n'y a pas ces forêts, ce ciel, cette lumière…[line break]".

Chapter 2 - Silex

Some silex are a thing in the montagneo.
The description is "Des pierres à feu, qui peuvent laisser échapper une étincelle si je les frotte.".

Instead of rubbing the silex:
	if a random chance of 1 in 3 succeeds
	begin;
		say "J'entrechoque les silex, et une étincelle jaillit[if the location is souterrain]. Par chance, elle tombe sur une sorte de liquide à terre, qui prend feu[end if].";
		if the location is souterrain, now the souterrain is lighted;
	else;
		say "J'ai beau les entrechoquer, l[']étincelle ne vient pas.";
	end if.
	
Part 13 - Montagnee

The montagnee is east from the montagneo, northeast from the forêt2 and north from the source. "Les contreforts de la chaîne qui se dresse juste devant moi et qui continue à l'ouest. Ce serait pure folie de s'aventurer plus loin. Je devrais revenir vers le sud.".
The printed name is "Au pied des montagnes".
The heat is 4.

Part 14 - Source

The source is north from the forêt and northeast from the ruines. "Un petit gargouillis sortant des rochers et formant un petit lac. Il a l'air profond[unless the player carries the orbe] ; je ferais bien de me reposer un peu ici avant de continuer[end unless]. D'ici, on peut voir les montagnes au nord.".
The printed name is "À la source".
The heat is 0.

Instead of swimming when the location is source, try downgoing.

Chapter 1 - Lac

The lacsc is scenery in source.
The printed name is "lac".
Understand "eau", "lac", "source" or "gargouillis" as the lacsc.
The description is "Un lac miroitant formé par la source. Je devrais peut-être plonger.".

Instead of entering the lacsc, try downgoing.

Part 15 - Lac

The lac is down from the source. "Sous l'eau, dans mon élément. [if the trappe is undescribed]Mis à part les poissons et les algues, il n'y a que des rochers ici. Je devrais remonter[else]Il y a une trappe qui était cachée par les algues ici[end if].".
The printed name is "Sous la surface de l'eau".
The heat is 0.

Chapter 1 - Poissons

Some poissons are scenery in the lac.
Understand "poisson" as the poissons.
The description is "De nombreux poissons nagent ici. Ils ne semblent pas se soucier des événements récents.".

Instead of taking or attacking the poissons, say "Je suis agile à la chasse, mais je dois me concentrer sur autre chose.".

Chapter 2 - Algues

Some algues are scenery in lac.
The description is "De longues algues poussant sur le fond du lac[if the trappe is undescribed]. Bizarrement, il y en a plus à un endroit[end if].".

Instead of taking or pulling the algues:
	if the trappe is undescribed, try searching the algues;
	else say "Je n'ai pas le temps de les arracher.".

Instead of searching the algues:
	say "Là ! il y a une trappe cachée sous les algues !";
	now the trappe is not undescribed.

Before eating the algues:
	say "Je ne connais pas cette espèce et je ne sais pas si elle est comestible.";
	stop the action.

Chapter 3 - Trappe

The trappe is a locked closed female undescribed door. The trappe is inside from the lac and outside from the souterrain.
The description is "Une trappe qui a gardé sa solidité malgré sa pourriture. Elle possède une serrure.".

Instead of doing something to the undescribed trappe, say "Je ne vois rien de tel, ou bien c'est sans grande importance.".

Instead of attacking the trappe, say "Mes efforts sont vain : elle est trop solide.".

Part 16 - Souterrain

The description of the souterrain is "Une galerie fraîche et humide. Elle semble artificielle et d'antiques étais la soutiennent encore du mieux qu'ils peuvent. Le souterrain se poursuit vers l'ouest. À moins que j'aie besoin de sortir.".
The printed name of the souterrain is "Le long d'un souterrain".
The souterrain is dark.
The heat is 1.

Part 17 - Sanctuaire

The sanctuaire is west from the souterrain. "Un immense dôme de pierre surplombe cette immense cavité rocheuse. Au centre, sur un magnifique piédestal ouvragé et doré à l'or fin, je l'ai enfin trouvé…".
The printed name is "Dans le sanctuaire".
The heat is 2.

Chapter 1 - Autel

The autel is a supporter in the sanctuaire. "L'Autel de la Lumière est là, devant moi.".
The printed name is "Autel de la Lumière". The indefinite article of the autel is "l[']".
Understand "piedestal" as the autel.
The description is "L'Autel de la Lumière. Il n'y a pas d'autres mots pour le décrire. Il est magnifique."

After examining the autel for the first time:
	say "Soudain, l'air au-dessus de l'Autel se trouble.";
	wait for any key;
	say "Un spectre apparaît alors. Un spectre de Nâga ! Celui-ci prend la parole avant que je puisse réagir :";
	wait for any key;
	say "[paragraph break]« Bienvenue en ces lieux. Tu dois être celui dont parle la prophétie. Vois-tu, c'est moi qui l'ai annoncée, alors que notre monde s[']écroulait.";
	wait for any key;
	say "[line break]L'Antique Démon de la Terre était en train de se réveiller dans une rage destructrice, après cent mille ans de sommeil. Mon peuple pria alors la Déesse du Ciel, lui demandant de vaincre le Démon.";
	wait for any key;
	say "[line break]Elle entendit nos prières, et le combat fut titanesque. Après des années de lutte, notre Déesse gagna enfin. Mais les conséquences furent terribles… » Le fantôme marque une pause, perdu dans ses pensées.";
	wait for any key;
	say "[line break]« Les continents avaient tous sombré dans les abysses, et nous fûmes obligés de quitter définitivement la surface… Oui, tu as compris, mon peuple et le tien ne font qu'un. Tu es ici dans le premier temple nâga.";
	wait for any key;
	say "[line break]Mais avant notre départ, la Déesse me donna l'Orbe Étincelante, seule arme capable de vaincre le Démon, quand il se réveillerait de nouveau après cent mille autres années de torpeur.";
	wait for any key;
	say "[line break]Cette histoire est passée dans le mythe ; seule la prophétie fut transmise à chaque génération. Et maintenant, elle se réalise. Va, Sauveur du monde ! Je te remets l'Orbe ! »";
	wait for any key;
	say "[paragraph break]Soudain la terre tremble, au moment même où le spectre disparaît. Vite, je dois retourner parmi les miens !";
	now the player carries the orbe.
	
Chapter 2 - Orbe

The orbe is a female thing.
The printed name is "Orbe Étincelante".
The indefinite article is "l[']".
The description is "L'Orbe Étincelante, seule arme capable de vaincre le Démon de la Terre. Elle brille de mille éclats et dégage une douce chaleur.".

Instead of dropping the orbe, say "Je dois la garder à tout prix !".
Instead of inserting the orbe into something, try dropping the orbe.
Instead of putting the orbe on something, try dropping the orbe.

Part 18 - Incendie

Forest is a region. Forêt, forêt2 and ruines are in forest.
	
Instead of going to forest when the player carries the orbe, say "Un incendie s'est déclaré dans toute la forêt ! Je dois trouver un autre passage !".
	
Incendie is a scene. Incendie begins when the player carries the orbe and the location is source.

When incendie begins:
	say "Le volcan de l[']île est entré en éruption et un incendie a gagné la forêt ! Je dois vite rejoindre l'océan !".
	
Every turn during incendie:
	unless the location is the océan or the location is lac or the location is souterrain or the location is sanctuaire, say "[one of]Vite[or]Quelle chaleur[or]Je dois rejoindre l'océan[at random] !";
	if the heat of the location is not 0, decrease the hydratation of the player by 5.

Volume 3 - Release
	
The story headline is "Une exploration interactive".
The story genre is "Fantasy".
The release number is 3.
The story description is "Pour sauver le monde du chaos provoqué par l'éveil du Démon de la Terre, vous, membre du peuple Nâga, devez vous rendre à la surface et trouver, au coeur d'une île déserte, l'Autel de la Lumière, où la solution sera dévoilée...".
The story creation year is 2011.

Release along with cover art, a solution, the source text and a file of "Changelog" called "LTN-changelog.txt".

Book 1 - Commandes

Part 1 - Aide

Understand "aide", "infos", "info", "informations" or "information" as a mistake ("Ce jeu est une fiction interactive ; il se joue en tapant des commandes à l'infinitif telles que « voir », « prendre [italic type]objet[roman type] », etc. (pour plus d'informations, consultez [italic type]http://ifiction.free.fr/[roman type]).[line break]Tapez « licence » pour l'obtenir et « auteur » pour en savoir plus sur l'auteur.").

Part 2 - Licence

Understand "licence" or "license" as a mistake ("[bold type]Pour faire bref :[line break][roman type]Ce programme est un logiciel libre ; vous pouvez le redistribuer ou le modifier suivant les termes de la « GNU General Public License » telle que publiée par la Free Software Foundation : soit la version 3 de cette licence, soit toute version ultérieure.[paragraph break]Ce programme est distribué dans l'espoir qu'il vous sera utile, mais SANS AUCUNE GARANTIE : sans même la garantie implicite de COMMERCIALISABILITÉ ni d'ADÉQUATION À UN OBJECTIF PARTICULIER. Consultez la Licence Générale Publique GNU pour plus de détails.[paragraph break][bold type]Pour faire long, [roman type]consultez : [italic type]http://www.gnu.org/licenses[roman type].").

Part 3 - Auteur

Understand "credits", "credit", "auteur" or "auteurs" as a mistake ("C'est en fouillant des ruines anciennes que l'auteur, Natrium729, a découvert d'antiques récits, relatant les aventures du Nâga qui sauva le monde jadis. Il a cru bon de les partager, et c'est ainsi qu'est né ce jeu. Il est cependant difficile de comprendre toute les subtilités de cette langue disparue, c'est pourquoi de l'aide pour déchiffrer ces écrits est disponible sur son site, [italic type]http://ulukos.com[roman type]. De plus, il est possible de le contacter à [italic type]natrium729@gmail.com[roman type]. Il remercie les personnes qui l'ont aidé à transcrire le texte, notamment les membres de [italic type]ifiction.free.fr[roman type].[line break]Natrium729 continue en ce moment ses recherches, car l'aventure continue dans [italic type]Entre Terre et Ciel[roman type] !").