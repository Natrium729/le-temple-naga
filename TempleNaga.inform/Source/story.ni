"Le Temple nâga" by Nathanaël Marion (in French)

[
© Nathanaël Marion

Pour faire bref :
Ce programme est un logiciel libre ; vous pouvez le redistribuer ou le modifier suivant les termes de la « GNU General Public License » telle que publiée par la Free Software Foundation : soit la version 3 de cette licence, soit toute version ultérieure.

Ce programme est distribué dans l'espoir qu'il vous sera utile, mais SANS AUCUNE GARANTIE : sans même la garantie implicite de COMMERCIALISABILITÉ ni d'ADÉQUATION À UN OBJECTIF PARTICULIER. Consultez la Licence Générale Publique GNU pour plus de détails.

Pour faire long, consultez : http://www.gnu.org/licenses
]

Volume 1 - AVANT TOUT

[Cette partie de la syntaxe ne fonctionne pas si on la met dans l'extension Experimental Frech Features. On est obligé de la mettre ici.]
Include (-
language French

<control-structure-phrase> ::=
	/a/ si ... est début |
	/b/ si ... est |
	/c/ si ... |
	/c/ à moins que |
	/d/ répéter ... |
	/e/ tant que ... |
	/f/ sinon |
	/g/ sinon si ... |
	/g/ sinon à moins que |
	/h/ -- sinon |
	/i/ -- ... |
	/j/ let ... be the phrase ...		[???]
			
-) in the Preform grammar.

Include Experimental French Features by Nathanael Marion.

Volume 2 - Initialisation

Book 1 - Basic Screen Effects

Include Basic Screen Effects by Emily Short.

Part 1 - Barre de statut

Remplir la barre de statut est initialement faux.

Table of Status Bar
left	central	right
"Tour [turn count]"	"[the player's surroundings]"	"Hydratation :"
""	"[si mode daltonien est vrai]([température])[fin si]"	"[hydratation]"

Rule for constructing the status line:
	si remplir la barre de statut est vrai, remplir la barre de statut avec Table of Status Bar;
	la règle réussit.

Book 2 - Glulx Text Effects

Part 1 - Dans la fenêtre principale

Include Glulx Text Effects by Emily Short.

Le bleu en hexa est toujours "3400DE".
Le vert en hexa est toujours "009819".
Le jaune en hexa est toujours "CD8700".
Le rouge en hexa est toujours "E00000".

Table of User Styles (continued)
style name	color	fixed width	font weight	indentation	first line indentation	italic	justification	relative size	reversed
blockquote-style	bleu en hexa	faux	bold-weight	0	0	faux	left-justified	1	faux
note-style	vert en hexa	faux	bold-weight	0	0	faux	left-justified	1	faux
special-style-1	jaune en hexa	faux	bold-weight	0	0	faux	left-justified	1	faux
special-style-2	rouge en hexa	faux	bold-weight	0	0	faux	left-justified	1	faux

Part 2 - Dans la barre de statut

Include Glulx Status Line Text Effects by Nathanael Marion.

[Les couleurs dans la barre de statut sont plus claires, pour que ce soit plus lisible sur fond noir.]
Le bleu-statut en hexa est toujours "5494fe".
Le vert-statut en hexa est toujours "00C232".
Le jaune-statut en hexa est toujours "F7CB00".
Le rouge-statut en hexa est toujours "F50000".

Table of Status Line User Styles (continued)
style name	background color	fixed width	font weight	indentation	first line indentation	italic	justification	relative size	reversed
blockquote-style	bleu-statut en hexa	vrai	regular-weight	0	0	faux	left-justified	0	vrai
note-style	vert-statut en hexa	vrai	regular-weight	0	0	faux	left-justified	0	vrai
special-style-1	jaune-statut en hexa	vrai	regular-weight	0	0	faux	left-justified	0	vrai
special-style-2	rouge-statut en hexa	vrai	regular-weight	0	0	faux	left-justified	0	vrai

To dire en bleu:
	dire "[style de citation en bloc]".
To dire en vert:
	dire "[style de note]".
To dire en jaune:
	dire "[premier style personnalisé]".
To dire en rouge:
	dire "[second style personnalisé]".

Book 3 - Senses

Include Senses by Nathanael Marion.

Book 4 - Actions

Part 1 - Nager

Swimming is an action applying to nothing.
Understand "swim" or "nager" as swimming.
Understand the command "dive" as "swim".
Understand the command "plonger" as "nager".

Check swimming (this is the block swimming rule):
	say "Il n'y a pas assez d'eau pour nager ici." (A) instead.

Part 2 - Se reposer

Resting is an action applying to nothing.
Comprendre "reposer vous" comme resting.

Check resting:
	si température de la location >= 4, dire "Il fait bien trop chaud pour que je me repose." instead.

Carry out resting:
	augmenter l' hydratation du player de 1.

Report resting:
	dire "Je me repose un instant.".

Book 5 - Règles

Part 1 - Obscurité

Instead of going from un endroit sombre to un endroit non-visité:
	dire "Je ne verrai pas par où je passe dans ce noir d'encre.".

Instead of going nulle part quand dans l'obscurité:
	dire "Je ne verrai pas par où je passe dans ce noir d'encre.".

Part 2 - Hors de portée

Une chose peut être hors de portée ou proche. Une chose est généralement proche.
Une chose has a text called interaction lointaine. L' interaction lointaine d'une chose est généralement "".

The can't touch out of reach things rule is listed before the access through barriers rule in the accessibility rulebook.

Accessibility rule (this is the can't touch out of reach things rule): 
	si l'action requiert un nom touchable et le noun est hors de portée:
		si l' interaction lointaine du noun est empty:
			dire "[Le noun] [es] trop loin." instead;
		sinon:
			dire "[interaction lointaine du noun][à la ligne]" instead;
	sinon si l'action requiert un second nom touchable et le second noun est hors de portée:
		si l' interaction lointaine du second noun est empty:
			dire "[Le second noun] [es] trop loin." instead;
		sinon:
			dire "[interaction lointaine du second noun][à la ligne]" instead.

Book 6 - Joueur

La description du player est "Je suis un jeune Nâga ordinaire, qui vient de finir mes neuf mues, et mes écailles bleues indiquent maintenant que je suis un adulte. Choisi par la prophétie, je dois maintenant m'aventurer hors de tout ce que je connaissais pour sauver mon peuple. Je n'ai pas de temps à perdre.".
Le sound du player est "J'entends mon cœur qui bat, rempli d'appréhension devant l'inconnu qui m'attend.".
Le scent du player est "Une odeur qui vient des profondeurs de l'océan.".
La texture du player est "Couvert d'écaille et [si hydratation du player > 50] encore humide[sinon] trop sec à mon goût[fin si].".
Le taste du player est "J'ai encore le goût salé de l'océan.".

[Sinon le joueur ne peut pas voir sa texture.]
The report touching yourself rule is not listed in any rulebook.
The report touching other people rule does nothing when the noun is the player.

Part 1 - Hydratation et température

Chapter 1 - Hydratation

Le player has un nombre called hydratation. L' hydratation du player est 100.

To dire hydratation:
	si hydratation du player <= 30:
		si le mode daltonien est faux:
			dire "[en rouge]";
		sinon:
			dire "! ";
	dire "[hydratation du player] %";
	si le mode daltonien est faux, dire "[romain]".

Chapter 2 - Température

Un endroit has un nombre called température. La température d'un endroit est généralement 10.

To say température:
	let T be la température de la location;
	si T >= 7:
		dire "chaud";
	sinon si T >= 4:
		dire "normal";
	sinon si T >= 1:
		dire "frais";
	sinon si T est 0:
		dire "de l'eau";

Every turn:
	si la température de la location est 0:
		maintenant l' hydratation du player est 100;
	sinon:
		diminuer l' hydratation du player par la température de la location;
		si hydratation du player <= 0:
			maintenant l' hydratation du player est 0;
			dire "Je ne sens plus mon corps sous le poids écrasant de la chaleur. Mes os blanchiront à jamais ici, et le monde sombrera dans le chaos…";
			attendre une touche;
			fin de l'histoire en disant "C'est la fin[_]!".

Before printing the name of un endroit (appelé E):
	si le mode daltonien est faux:
		si température de E >= 7:
			dire "[en rouge]";
		sinon si température de E >= 4:
			dire "[en jaune]";
		sinon si température de E >= 1:
			dire "[en vert]";
		sinon si la température de E est 0:
			dire "[en bleu]".

Before printing the name of a dark room:
	si le mode daltonien est faux:
		si température de la location >= 7:
			dire "[en rouge]";
		sinon si température de la location >= 4:
			dire "[en jaune]";
		sinon si température de la location >= 1:
			dire "[en vert]";
		sinon si la température de la location est 0:
			dire "[en bleu]".

After printing the name of un endroit (appelé E) while not constructing the status line:
	si le mode daltonien est vrai, dire " ([température])".

After printing the name of un endroit (appelé E):
	si le mode daltonien est faux, dire "[romain]".

After printing the name of a dark room:
	si le mode daltonien est vrai, dire " ([température])".

After printing the name of a dark room while not constructing the status line:
	si le mode daltonien est faux, dire "[romain]".

Chapter 3 - Mode daltonien

Le mode daltonien est un booléen qui varie. Le mode daltonien est faux.

Section 1 - Activer les couleurs

Activating colours is an action out of world.
Comprendre "couleurs on/marche" comme activating colours.
Comprendre "activer les/-- couleurs" comme activating colours.
Understand the command "couleur" as "couleurs".

Check activating colours:
	si le mode daltonien est faux, dire "Les couleurs sont déjà activées." instead.

Carry out activating colours:
	maintenant le mode daltonien est faux.

Report activating colours:
	dire "Le nom des lieux va maintenant s'afficher en couleur.".

Section 2 - Désactiver les couleurs

Deactivating colours is an action out of world.
Comprendre "couleurs off/arret" comme deactivating colours.
Comprendre "desactiver les/-- couleurs" comme deactivating colours.

Check deactivating colours:
	si le mode daltonien est vrai, dire "Les couleurs sont déjà désactivées." instead.

Carry out deactivating colours:
	maintenant le mode daltonien est vrai.

Report deactivating colours:
	dire "La température des lieux va maintenant s'afficher entre parenthèses après leur nom, et les couleurs ne seront plus utilisées.".

Section 3 - Alterner les couleurs

Toggling colours is an action out of world.
Comprendre "couleurs" comme toggling colours.

Check toggling colours:
	si le mode daltonien est vrai, convert to the activating colours action on nothing;
	sinon convert to the deactivating colours action on nothing;

Section 4 - Tester les couleurs

Testing colours is an action out of world applying to nothing.
Comprendre "couleurs test" comme testing colours. 
Comprendre "tester les/-- couleurs" comme testing colours.

Report testing colours:
	dire "Si chaque ligne ne s'affiche pas dans la couleur indiquée, désactivez-les ou changez d'interpréteur[_]![saut de paragraphe][en bleu]Ce texte est en bleu.[à la ligne][en vert]Ce texte est en vert.[à la ligne][en jaune]Ce texte est en jaune.[à la ligne][en rouge]Ce texte est en rouge.[romain][à la ligne]"

Chapter 4 - Informations

After printing the banner text:
	dire "[première fois]Tapez «[_]aide[_]» pour obtenir plus d'informations sur le jeu ou si vous jouez à une fiction interactive pour la première fois.[saut de paragraphe][crochet]Note[_]: L'hydratation du héros [--] notée en haut à droite [--] correspond à l'eau qu'il reste à son corps.[à la ligne]Chaque tour, elle baisse en fonction de la température de l'endroit, qui est indiquée par la couleur du nom du lieu. Le bleu en particulier indique que l'endroit contient de l'eau[_]; l'hydratation remontera alors à 100[_]%. Le héros meurt si elle atteint 0.[saut de paragraphe]Si, pour une raison ou une autre, vous n'êtes pas en mesure de voir les couleurs, tapez «[_]couleurs arrêt[_]»[_]; tapez «[_]tester les couleurs[_]» pour vérifier si les couleurs fonctionnent. Pour plus de détails, tapez «[_]aide[_]».[crochet fermant][à la ligne][seulement]";

Book 7 - Langue

Part 1 - Nouveaux verbes

In French couler is a verb.

Volume 3 - Jeu

Book 1 - Introduction

When play begins:
	maintenant le story viewpoint est la première personne du singulier;
	dire "[italique]Le fond de l'océan avait tremblé cette nuit-là. Effrayés par ce phénomène encore nouveau pour nous, nous crûmes à la fin du monde.[saut de paragraphe]";
	attendre une touche;
	dire "Nous nous réunîmes alors devant le temple sacré de notre Déesse, où la prêtresse tint ces mots[_]:[saut de paragraphe]";
	attendre une touche;
	dire "«[_]La prophétie est sur le point de se réaliser, et le démon enfoui au plus profond des entrailles de la Terre se réveille.[_]» déclara-t-elle gravement.[saut de paragraphe]";
	attendre une touche;
	dire "Nous restâmes muets de terreur[_]; on aurait pu entendre une méduse nager. Mais elle continua[_]:[saut de paragraphe]";
	attendre une touche;
	dire "«[_]Cependant, il reste un espoir. Le plus jeune d'entre nous ayant déjà fini ses neuf mues devra se rendre dès maintenant à la surface, sur l'île la plus proche. En ces lieux, il devra trouver l'Autel de la Lumière. Là lui sera révélée la solution.[_]»[saut de paragraphe]";
	attendre une touche;
	dire "Il y eut des murmures dans l'assemblée. Le sauveur du peuple Nâga choisi par la prophétie, c'était moi. Sous les prières alors psalmodiées, je nageai vers la surface.[saut de paragraphe]";
	attendre une touche;
	dire "Encore et toujours, sans m'arrêter.[romain][à la ligne]";
	attendre une touche;
	maintenant remplir la barre de statut est vrai.

Book 2 - Jeu

Part 1 - Océan

L' océan (m) est un endroit. "Le soleil s'est levé il y a quelques heures sur cet océan à perte de vue. La mer est calme, comme si elle avait déjà oublié les événements de la nuit dernière. Ma patrie se trouve sous moi, [si le player porte l' orbe]je dois plonger[sinon]mais il me faut continuer[_]: je vois la ligne d'une plage au nord[fin si]."
Le printed name est "Parmi les vagues".
Le sound est "Le bruit des vagues est apaisant, comme si rien ne s'était passé.".
Le scent est "L'odeur de la mer, mais pas celle des profondeurs[_]: celle de la surface.".
La température est 0.

Instead of going nulle part from l' océan when le noun n'est pas haut and le noun n'est pas bas:
	si l' incendie est en train de se dérouler, dire "Je dois plonger[_]! Vite[_]!";
	sinon dire "Non, je dois atteindre l'île.".

Instead of going bas dans l' océan:
	si le player porte l' orbe, essayer de swimming;
	sinon dire "Non, je dois atteindre l'île.".

Instead of jumping quand la location est l' océan ou la location est le lagon ou la location est le fond:
	dire "Je suis en train de nager.".

Instead of dropping quelque chose quand la location est l' océan ou la location est le lagon, dire "Si je pose [le noun] ici, je ne pourrai plus [le] récupérer.". 

Instead of swimming dans l' océan quand le player porte l' orbe:
	dire "Sans perdre un instant, je plonge, l'Orbe dans mes mains. Je nage du mieux que je peux, mais la fatigue me rattrape. Non, je dois réussir[_]! Vite, il faut que j'arrive à temps[_]!";
	attendre une touche;
	fin définitive de l'histoire en disant "À suivre…".

Instead of swimming quand la location est l' océan ou la location est le lagon ou la location est le fond:
	dire "Je suis déjà en train de nager.".

Chapter 1 - Île

L' île (f) est une chose décorative hors de portée dans l' océan.
La description est "L'île que je dois atteindre.".
L' interaction lointaine est "Je devrais m'approcher.".

Comprendre "ile", "ligne" ou "plage" comme l' île.

Instead of examining le nord dans l' océan, essayer de examining l' île.

Chapter 2 - Eau

L' eau (m) est une toile de fond. L' eau est dans l' océan, le lagon, la falaise-sud, la falaise-nord, la plage-milieu, la plage-ouest et la plage-est.
Le printed name est "océan".
La description est "Une étendue d'eau salée rejoignant l'horizon.".
Le sound est "Je plonge un instant la tête sous l'eau. Le silence qui m'enveloppe alors m'apaise un instant.".
Le scent est "Lorsqu'on est sous l'eau, elle possède une odeur particulière, subtile.".
La texture est "Liquide, bien évidemment.".
Le taste est "Salé.".
L' interaction lointaine est "L'océan est trop loin en contrebas.".

Comprendre "ocean", "mer" ou "vagues/vague" comme l' eau.

Carry out going to la plage-ouest:
	maintenant l' eau est hors de portée.
Carry out going to la falaise-nord:
	maintenant l' eau est hors de portée.
Carry out going to la falaise-sud:
	maintenant l' eau est hors de portée.

Carry out going from la plage-ouest:
	maintenant l' eau est proche.	
Carry out going from la falaise-nord:
	maintenant l' eau est proche.
Carry out going from la falaise-sud:
	maintenant l' eau est proche.

Instead of outgoing l' eau quand la location est l' océan ou la location est le lagon, essayer de going nord.

Instead of looking under l' eau:
	dire "Seul un dieu pourrait soulever l'océan et regarder ce qu'il y a en dessous.".

Instead of taking l' eau:
	si le player porte le galet ou le player porte la pelle, dire "J'ai de quoi transporter de l'eau, mais il serait difficile de me déplacer ensuite.";
	sinon dire "Je n'ai rien pour transporter de l'eau.".

Instead of entering l' eau:
	si la location est la plage-milieu ou la location est la plage-est:
		essayer de going sud;
	sinon:
		essayer de swimming.

Before entering l' eau quand la location est la falaise-sud ou la location est la falaise-nord ou la location est la plage-ouest:
		essayer de jumping instead;

Instead of searching l' eau:
	si la player's command contient "creuser":
		dire "Creuser de l'eau[_]?";
	sinon:
		dire "Fouiller l'océan. Même les plus grands aventuriers n'ont pas réussi.".

Before eating l' eau:
	dire "Cela ne se mange pas[_]!";
	arrêter l'action.

Instead of drinking l' eau:
	si la location est la falaise-sud ou la location est la falaise-nord ou la location est la plage-ouest:
		essayer de jumping;
	sinon si la location est la plage-milieu ou la location est la plage-est:
		dire "Je me penche et plonge un instant ma tête sous l'eau, laissant le liquide passer dans mes branchies.";
	sinon:
		dire "Être dans mon élément me suffit.".

Instead of inserting quelque chose into l' eau:
	dire "Il valait mieux garder [le noun] avec moi, car je risquerais de ne plus [le] retrouver dans l'eau.".

Instead of throwing quelque chose at l' eau:
	dire "Futile.".

Instead of pushing ou pulling l' eau:
	dire "Je ne peux pas faire cela[_]!".

Chapter 3 - Soleil

Le soleil est une toile de fond hors de portée. Le soleil est dans l' océan, le lagon, la falaise-sud, la falaise-nord, la plage-milieu, la plage-ouest, la plage-est, la source, la montagne-ouest et la montagne-est.
La description est "Cette boule lumineuse et brûlante ne possède pas d'équivalent là d'où je viens et elle me dessèche. Trouvons de l'ombre au plus vite.".

Part 2 - Lagon

Le lagon est au sud de la plage-est. "Un magnifique lagon turquoise, où il fait bon nager. La barrière de corail m'empêche de rejoindre l'océan, mais de toute façon, je ne dois pas me détourner de mon objectif[_]; je ferais mieux de revenir sur la terre ferme au nord.".
Le printed name est "Dans l'eau du lagon".
Le sound est "Le bruit des vagues est apaisant, comme si rien ne s'était passé.".
Le scent est "L'odeur de la mer, mais pas celle des profondeurs[_]: celle de la surface.".
La température est 0.

Instead of going bas quand la location est le lagon, dire "Je plonge un instant, mais reviens vite à la surface[_]: je ne dois pas perdre de temps.".

Instead of going sud dans le lagon:
	dire "La barrière de corail m'empêche de rejoindre l'océan.".
Instead of going ouest dans le lagon:
	dire "La barrière de corail m'empêche de rejoindre l'océan.".
Instead of going sud-ouest dans le lagon:
	dire "La barrière de corail m'empêche de rejoindre l'océan.".
Instead of going est dans le lagon:
	dire "La barrière de corail m'empêche de rejoindre l'océan.".
Instead of going sud-est dans le lagon:
	dire "La barrière de corail m'empêche de rejoindre l'océan.".

Chapter 1 - Barrière de corail

La barrière de corail (f) est une chose décorative hors de portée dans le lagon.
La description est "Un immense mur sous-marin fait de coraux. D'où je me trouve, il m'empêche de rejoindre l'océan.".
L' interaction lointaine est "M'approcher de la barrière de corail serait un gaspillage de temps.".

Comprendre "barriere" comme la barrière de corail.

Part 3 - Plage du milieu

La plage-milieu est au nord de l' océan. "Une longue étendue de sable blanc, qui se poursuit à l'est et à l'ouest. La chaleur est étouffante[à moins que le player porte l' orbe]. Je devrais rejoindre l'abri que m'offrent les arbres au nord et au nord-ouest[_]; en effet, la jungle succède directement à cette plage ondulée[fin à moins que]."
Le sound est "Malgré la chaleur, je me laisse bercer par le bruit des vagues.".
Le scent est "L'odeur de la mer, mais pas celle des profondeurs[_]: celle de la surface.".
Le printed name est "Sur le sable".

Instead of swimming quand la location est la plage-milieu, essayer de going sud.

Chapter 1 - Sable

Le sable est une toile de fond. Le sable est dans la plage-milieu et la plage-est.
La description est "Un sable blanc et brûlant. Je ne devrais pas m'attarder.".
Le sound est "Le sable fait un drôle de bruit que je me meus.".
Le scent est "Pour que des grains de sable me brûlent de l'intérieur[_]?".
La texture est "Jamais je ne mettrai mes mains dans ce sable brûlant[_]!".
Le taste est "Jamais[_]!".

Comprendre "plage" comme le sable.

Instead of taking le sable:
	essayer de touching le sable.

Comprendre "creuser [quelque chose]" ou "creuser" comme searching.

Rule for supplying a missing noun while searching when le sable est touchable:
	maintenant le noun est le sable.

Rule for supplying a missing noun while searching:
	maintenant le noun est la location.

Instead of searching quand le noun est un endroit:
	dire "Creuser à cet endroit sera difficile et, selon toute vraisemblance, une perte de temps.".

Instead of searching le sable:
	si le player porte le galet:
		dire "Le galet pourrait m'aider, mais je n'ai rien pour le tenir pendant que je creuse[_]: il me faudrait un manche.";
	sinon si le player ne porte pas la pelle:
		dire "Je n'ai rien qui me permettrait de creuser. Et jamais je ne mettrai mes mains dans ce sable brûlant[_]!";
	sinon si la location est la plage-est et le coffret est hors scène:
		dire "Sous la torride chaleur du soleil, je creuse, transpirant à grosses gouttes et perdant beaucoup d'eau. Enfin, ma pelle bute contre quelque chose de dur. Un coffret[_]!";
		diminuer l' hydratation du player de 20;
		maintenant le coffret est dans la plage-est;
		établir les pronoms pour le coffret;
	sinon:
		dire "Je creuse pendant un court instant autour de moi, mais je ne trouve rien d'intéressant." .

Chapter 2 - Forêt

La forêt est une toile de fond hors de portée. La forêt est dans la plage-milieu, la plage-ouest, la plage-est, la falaise-nord, la montagne-ouest et la montagne-est.
La description est "Une forêt dense mais ombragée. Je devrais peut-être m'y abriter.".

Comprendre "foret/jungle", "arbre/arbres" ou "branches" comme la forêt.

Instead of examining le nord dans la plage-milieu, essayer de examining la forêt.
Instead of examining le nord-ouest dans la plage-milieu, essayer de examining la forêt.
Instead of examining le nord dans la plage-ouest, essayer de examining la forêt.
Instead of examining le nord-est dans la plage-ouest, essayer de examining la forêt.
Instead of examining le nord-ouest dans la plage-est, essayer de examining la forêt.
Instead of examining est dans la falaise-nord, essayer de examining la forêt.
Instead of examining le sud-est dans la falaise-nord, essayer de examining la forêt.
Instead of examining le sud dans la montagne-ouest, essayer de examining la forêt.
Instead of examining le sud-est dans la montagne-ouest, essayer de examining la forêt.
Instead of examining le sud dans la montagne-est, essayer de examining la forêt.
Instead of examining le sud-ouest dans la montagne-est, essayer de examining la forêt.

Chapter 3 - Coffret

Le coffret est un contenant fermé ouvrable. "Le petit coffret que j'ai déterré gît près du trou d'où il vient.".
La description est "Un vieux coffret, un peu abîmé par le temps. Son loquet est tout rouillé et je n'aurai sûrement aucun mal à l'ouvrir.".
Le sound est "Le loquet grince un peu quand on le manipule.".
Le scent est "Le coffret sent le bois humide et la pourriture.".
La texture est "Le bois est humide et abîmé, mais il semble encore assez solide.".
Le taste est "Ça a un goût de bois pourri.".

Before inserting quelque chose into le coffret:
	si le noun n'est pas la clef:
		dire "Le coffret a été conçu pour la clef, [le noun] n'y [entres] pas.";
		stop the action.

Chapter 4 - Clef rouillée

La clef rouillée ouvre la trappe. La clef est dans le coffret.
La description est "Une clef antique, rouillée par le temps. Je devrais relire le parchemin, peut-être apprendrai-je ce qu'elle ouvre[_]?".
Le scent est "La clef dégage une odeur de rouille, avec une pointe d'eau salée.".
La texture est "La clef est rugueuse et laisse de la rouille entre les doigts.".
Le taste est "Elle laisse un goût métallique dans la bouche. Déplaisant.".

Comprendre "cle" ou "rouillee" comme la clef.

Part 4 - Plage ouest

La plage-ouest est à l'ouest de la plage-milieu. "La plage laisse rapidement place à des rochers où viennent se briser les vagues. Ici, impossible de plonger[_]: ces pierres glissantes et tranchantes rendraient le retour à la terre ferme impossible. Je ne peux que retourner sur la plage à l'est, aller plus loin sur la côte au nord-ouest ou m'aventurer dans la jungle au nord et au nord-est.".
Le printed name est "Surplombant la mer".
Le sound est "On peut entendre le bruit des vagues se brisant contre les rochers.".
Le scent est "L'odeur de la mer est moins présente.".
La température est 8.

Instead of jumping quand la location est la plage-ouest:
	dire "Les rochers sont trop nombreux, et la houle est trop forte.".

Instead of swimming quand la location est la plage-ouest, essayer de jumping.
Instead of going bas dans la plage-ouest, essayer de jumping.

Chapter 1 - Galet

Le galet est une chose dans la plage-ouest. "Un étrange galet attire mon attention.".
La description est "Une étrange pierre creuse. On aurait presque dit un bol. Je me demande ce qui a pu lui donner cette forme.".
Le sound est "Je tapote le galet[_]; cela ne provoque pas de son particulier.".
Le scent est "Il a l'odeur de la mer et du vent qui l'ont battu.".
La texture est "Le galet est lisse et creux.".
Le taste est "Le galet n'a pas un goût très agréable.".

Comprendre "pierre", "caillou" ou "bol" comme le galet.

Instead of tying le galet to quelque chose, dire "Je devrais attacher le galet à autre chose.".
Instead of tying quelque chose to le galet, dire "Je devrais attacher le galet à autre chose.".

Chapter 2 - Rocs

Les rocs sont des choses décoratives hors de portée dans la plage-ouest.
La description est "Des rochers glissants et acérés brisent les vagues.".
L' interaction lointaine est "Il est trop risqué de m'aventurer sur ces rocs glissants.".

Comprendre "rochers" ou "pierres" comme les rocs.

Part 5 - Plage est

La plage-est est à l'est de la plage-milieu et au sud-est de la forêt-est. "De ce côté, le sable brille plus fort que jamais. Le soleil est écrasant. Je ne devrais pas m'attarder en ce lieu. Heureusement, l'océan n'est pas loin au sud[à moins que le player porte l' orbe], et le couvert des branches m'attend au nord-ouest[fin à moins que].".
Le printed name est "Sur le sable".
Le sound est "Malgré la chaleur, je me laisse bercer par le bruit des vagues.".
Le scent est "L'odeur de la mer, mais pas celle des profondeurs[_]: celle de la surface.".
La température est 11.

Instead of swimming quand la location est la plage-est, essayer de going sud.

Part 6 - Falaise sud

La falaise-sud est au nord-ouest de la plage-ouest, à l'ouest des ruines et au sud-ouest de la forêt-nord. "Une large falaise, s'étirant plus au nord, domine l'océan de toute sa hauteur. Il serait suicidaire de sauter[_]; je devrais [à moins que le player porte l' orbe]rentrer plus dans les terres à l'est ou au nord-est, ou bien [fin à moins que]regagner la plage au sud-est.".
Le printed name est "Loin au-dessus de l'eau".
Le sound est "Le son de la mer se fait lointain.".
Le scent est "L'odeur de la mer devient à peine perceptible à cette distance.".
La température est 7.

Instead of jumping quand la location est la falaise-sud ou la location est la falaise-nord:
	dire "Succombant à l'appel de l'océan, je saute, plonge et me fracasse contre le roc.";
	attendre une touche;
	fin de l'histoire en disant "C'est la fin[_]!".

Instead of swimming quand la location est la falaise-sud ou la location est la falaise-nord:
	essayer de jumping.

Part 7 - Falaise nord

La falaise-nord est au nord de la falaise-sud, au nord-ouest des ruines et à l'ouest de la forêt-nord. "Une haute falaise qui continue plus au sud. En contrebas, une mâchoire de rocs me broierait si je sautais[_]; au nord-est se dressent des montagnes[à moins que le player porte l' orbe]. Je ferais mieux de retourner dans la forêt à l'est ou au sud-est[fin à moins que].".
Le printed name est "Loin au-dessus de l'eau".
Le sound est "Le bruit des vagues est plus étouffé ici.".
Le scent est "À cette hauteur, l'odeur de la mer est bien moins présente.".
La température est 6.

Part 8 - Ruines

Les ruines (f) sont au nord de la plage-ouest, à l'ouest de la forêt-est et au nord-ouest de la plage-milieu. "D'étranges restes de bâtiments, abandonnés à la nature qui a repris ses droits. Bizarrement, les décombres me rappellent l'architecture de nos cités.[à la ligne]Seule, une construction de pierre semble tenir tête à la forêt tout entière. Je peux m'enfoncer dans la forêt au nord et à l'est, j'entends le son de l'eau au nord-est et la jungle s'éclaircit à l'ouest et au sud.".
Le printed name est "Dans les ruines".
Le sound est "Le calme de cet endroit est brisé par le son de l'eau, proche d'ici.".
Le scent est "La jungle a également reconquis cet endroit par son odeur.".
La température est 5.

Chapter 1 - Décombres

Les décombres (m) sont des choses décoratives dans les ruines.
La description est "Des débris moussus, témoins du village qui se dressait autrefois là. Quelle peut bien être la raison de sa disparition[_]?".
Le scent est "Ces décombres exhalent une odeur de mousse.".
La texture est "Je pose ma main sur un bloc proche et suis du doigt l'une des moulures. Ces motifs me sont étrangement familiers.".
Le taste est "Cette mousse est-elle comestible[_]? Mieux vaut ne pas chercher à savoir.".


Comprendre "decombres", "gravats", "restes", "ruines", "village", "ville" et "batiments" comme les décombres.

Chapter 2 - Construction

La construction est une chose décorative dans les ruines.
La description est "Une étrange bâtisse, faisant front à la nature par sa solidité. Je devrais l'explorer[_]; elle pourrait contenir un indice.".

Comprendre "batiment", "batisse", "maison" ou "pyramide" comme la construction.

Instead of entering la construction, essayer de going dedans.

Understand "explorer [la construction]" as entering when la location est les ruines.

Part 9 - Pyramide

La pyramide est à l'intérieur des ruines. "Une apaisante fraîcheur règne en ce lieu. Le silence ainsi que le poids du temps que dégage cet endroit forcent au respect. Tout ici semble détaché du reste du monde. Peut-être y aura-t-il un indice sur l'Autel de la Lumière[_]?".
Le printed name est "À l'intérieur de la pyramide".
Le sound est "Un silence apaisant règne dans cet endroit.".
Le scent est "Là aussi, la nature a repris ses droits, et l'odeur de la mousse qui recouvre les murs embaume l'endroit.".
La température est 2.

Rule for printing the name of la pyramide while constructing the status line:
	dire "Dans la pyramide".

Chapter 1 - Sarcophage

Le sarcophage est un contenant fermé ouvrable fixé sur place privately-named dans la pyramide. "Il y a [si le sarcophage n'a pas été ouvert]un étrange coffre en pierre, qui semble ne pas avoir bougé depuis des lustres. Serait-ce une bonne idée de l'ouvrir[_]?[sinon]un antique sarcophage qui ne semble pas avoir bougé depuis des lustres.[fin si]".
Le printed name est "[si le sarcophage n'a pas été ouvert]coffre en pierre[sinon]sarcophage[fin si]".
La description est "[si le sarcophage n'a pas été ouvert]Ce coffre en pierre imposant occupe le centre du bâtiment[sinon]Ce sarcophage, simple en apparence, trône au centre du bâtiment[fin si]. Des motifs sont gravés sur le couvercle, mais j'ai du mal à les distinguer dans la pénombre.".
Le scent est "[Le sarcophage] possède l'odeur de la vieillesse, du temps qui a passé.".
La texture est "Du bout des doigts, je suis les motifs qui recouvrent [le sarcophage]. J'ai l'impression de les connaître.".
Le taste est "C'est froid sur la langue.".

Comprendre "coffre", "en", "pierre", "couvercle" or "motif/motifs" comme le sarcophage.
Understand "sarcophage" as le sarcophage when le sarcophage a été ouvert.

Instead of looking under le sarcophage, dire "Il est trop lourd[_]; je ne peux pas le soulever.".

After opening le sarcophage pour la première fois:
	say "Je pousse le couvercle du coffre, qui se révèle être un sarcophage[_]: il contient une momie, qui tient entre ses mains desséchées un lambeau de parchemin.".

Instead of pushing le sarcophage fermé:
	essayer de opening le sarcophage.

Chapter 2 - Momie

La momie est une chose dans le sarcophage.
La description est "Cette momie s'est bien conservée malgré l'humidité. Elle semble inquiète, même dans la mort[si la carte est une partie de la momie].[à la ligne]Elle tient dans ses mains un lambeau de parchemin[fin si].".

Instead of doing something other than examining with la momie, say "Mon peuple respecte les morts, qui qu'ils soient.".

Chapter 3 - Carte

La carte (m) est une chose privately-named. La carte est une partie de la momie.
Le printed name est "lambeau de parchemin".
La description est "Une vieille peau tannée à manipuler avec précaution. Il semblait y avoir quelque chose d'écrit[_]:[saut de paragraphe][italique]Afin de prot … l'Aut … ière du … mon … erre … caché … lef … sanctuai … ource[romain][saut de paragraphe][carte]".
Le scent est "Une odeur de renfermé.".
La texture est "Le parchemin est abîmé et fragile.".
Le taste est "Mieux vaut ne pas l'abîmer.".

Comprendre "lambeau", "de" ou "parchemin" comme la carte.
Understand "carte"as la carte when we have examined la carte.

Instead of examining la carte quand la carte est une partie de la momie:
	dire "Il n'y a rien sur la face supérieure du parchemin. Il faudrait le prendre pour examiner l'autre côté.".

Instead of searching la carte quand la carte est une partie de la momie:
	dire "Il faudrait prendre le parchemin si je veux l'inspecter.".
Instead of looking under la carte quand la carte est une partie de la momie, essayer de searching la carte.

After examining la carte quand la carte n'est pas une partie de la momie et la carte est male:
	maintenant le printed name de la carte est "carte";
	maintenant le grammatical gender de la carte est le feminine gender;
	maintenant la carte est female.

Before taking la carte quand la carte est une partie de la momie:
	now la carte est dans le sarcophage.

After taking la carte pour la première fois:
	dire "Malgré mon appréhension, je détache délicatement le lambeau de parchemin des mains de la momie.".

Section 1 - L'image

To dire carte:
	si le mode daltonien est faux:
		dire "[largeur fixe]   ^                  ^[à la ligne]";
		dire "        ^       /\[à la ligne]";
		dire "     /\   /\   /  \[à la ligne]";
		dire "    /, \/\  \_/  _/\   ^[à la ligne]";
		dire "  ^| /\/. \/O\/\/ . |[à la ligne]";
		dire "   |/ .\  /   \ \/ \|[à la ligne]";
		dire "   | %&m &%& &§  &&%| ^[à la ligne]";
		dire "^  | $m&§&$§&m ^^ Y&|[à la ligne]";
		dire "   | Y  A  &%&Ç§&mB§|[à la ligne]";
		dire "   | &Çm&Y&§&Ç&§&-—-´   ^[à la ligne]";
		dire "   |              x S[à la ligne]";
		dire " ^ `—-~~~~~~~~~~~~~~´[à la ligne]";
		dire "      ^              ^[à la ligne]";
		dire "             ^   www[à la ligne][largeur variable]";
		dire "[à la ligne]On dirait une carte[_]!";
	sinon:
		dire "Un dessin ressemblant à une carte stylisée recouvre le reste du parchemin. Une croix y a été tracée en bas à droite."

Section 2 - Aide-mémoire - apparence de la carte

[AVANT :

To say carte:
	say fixed letter spacing;
	say blue letters;
	say "^^^^^^^^^^^^^^^^^^^^^^^^^[line break]^^^^^^^^^^^^^^^^^^^^^^^^^";
	say line break;
	say "^^^[default letters]|MMMMMMMMMMMMMMMMM|[blue letters]^^^";
	say line break;
	say "^^^[default letters]|MMMMMMM[magenta letters] O [default letters]MMMMMMM|[blue letters]^^^";
	say line break;
	say "^^^[default letters]|MMMMMMMMMMMMMMMMM|[blue letters]^^^";
	say line break;
	say "^^^[default letters]|[green letters]&&&&&&&&&&[blue letters]^^^[green letters]&&&[yellow letters] S[blue letters]^^^";
	say line break;
	say "^^^[default letters]|[green letters]&&&&&&&&&&&[blue letters]^^[green letters]&&&[yellow letters] S[blue letters]^^^";
	say line break;
	say "^^^[default letters]|[green letters]&&&[default letters] A [green letters]&&&&&&&&&&[yellow letters] S[blue letters]^^^";
	say line break;
	say "^^^[default letters]|[green letters]&&&&&&&&&&&&&&&&[yellow letters] S[blue letters]^^^";
	say line break;
	say "^^^[default letters]|              [red letters]x[yellow letters]  S[blue letters]^^^";
	say line break;
	say "^^^ [yellow letters]~~~~~~~~~~~~~~~~ [blue letters]^^^^";
	say line break;
	say "^^^^^^^^^^^^^^^^^^^^^^^^^[line break]^^^^^^^^^^^^^^^^^^^^^^^^^";
	say "[variable letter spacing][default letters]".]

[AVANT :

^^^^^^^^^^^^^^^^^^^^^^^^^
^^^^^^^^^^^^^^^^^^^^^^^^^
^^^|MMMMMMMMMMMMMMMMM|^^^
^^^|MMMMMMM O MMMMMMM|^^^
^^^|MMMMMMMMMMMMMMMMM|^^^
^^^|&&&&&&&&&&^^^&&& S^^^
^^^|&&&&&&&&&&&^^&&& S^^^
^^^|&&& A &&&&&&&&&& S^^^
^^^|&&&&&&&&&&&&&&&& S^^^
^^^|              x  S^^^
^^^ ~~~~~~~~~~~~~~~~ ^^^^
^^^^^^^^^^^^^^^^^^^^^^^^^
^^^^^^^^^^^^^^^^^^^^^^^^^


MAINTENANT :

   ^                  ^
        ^       /\
     /\   /\   /  \
    /, \/\  \ /  _/\   ^
  ^| /\/. \/O\/\/ . |
   |/ .\  /   \ \/ \|
   | %&m &%& &§  &&%| ^
^  | $m&§&$§&m ^^ Y&|
   | Y  A  &%&Ç§&mB§|
   | &Çm&Y&§&Ç&§&-—-´   ^
   |              x S
 ^ `—-~~~~~~~~~~~~~~S
      ^              ^
             ^   www
]

Part 10 - Forêt est

La forêt-est est au nord de la plage-milieu et au nord-est de la plage-ouest. "La température est plus supportable, mais je ne vois plus où je vais[_]: le feuillage dru cache le soleil et les arbres ne sont pas clairsemés. Il n'y a que la forêt, à l'ouest et au nord-ouest. J'entends cependant le clapotis de l'eau plus au nord et les arbres s'éclaircissent vers le sud.".
Le printed name est "Entre les arbres".
Le sound est "La forêt est emplie de sons nouveaux et inquiétants[_]: craquements de branches, bruits de créatures tapies dans l'ombre… Heureusement, un clapotis rassurant se fait entendre depuis le nord.".
Le scent est "La forêt est remplie d'odeurs étranges et enivrantes[_]: senteurs des feuilles, du sous-bois…".
La température est 4.

Chapter 1 - Arbres

Les arbres sont des toiles de fond. Les arbres sont dans la forêt-est, la forêt-nord, les ruines et la source.
La description est "D'immenses troncs serrés, rendant le parcours difficile. Heureusement, les hautes frondaisons me protègent plus ou moins du soleil.".
La texture est "L'écorce est rugueuse sous la mousse.".
Le taste est "Je préférerais éviter de goûter à toutes les plantes inconnues que je trouve sur mon chemin.".

Comprendre "arbre", "jungle", "foret", "feuilles" ou "feuillage" comme les arbres.

Instead of climbing les arbres:
	dire "Peut-être que de là-haut, j'aurais une meilleure vue de l'île[_]? Je commence donc mon ascension…[à la ligne]";
	attendre une touche;
	dire "Sous l'eau, on ne peut rien escalader, alors je ne sais pas trop m'y prendre. Je glisse, et tombe violemment au sol.";
	attendre une touche;
	fin de l'histoire en disant "C'est la fin[_]!".

Instead of entering les arbres:
	essayer de climbing les arbres.

Instead of listening to les arbres, essayer de listening to la location.
Instead of smelling les arbres, essayer de smelling la location.

Instead of examining une direction quand les arbres sont visible:
	dire "Les arbres m'empêchent de voir très loin ici.".

Instead of examining le haut quand les arbres sont visible:
	dire "Les branches cachent le soleil, ce qui n'est pas pour me déplaire.".

Chapter 2 - Liane

La liane est une chose dans la forêt-est. "Une liane traîne par terre.".
La description est "Un morceau de liane commençant à pourrir, et qui a certainement dû tomber d'un arbre.".
Le scent est "Elle exhale un parfum végétal, différent des plante sous-marines.".
La texture est "Elle est un peu visqueuse, collante.".
Le taste est "Je préfère ne pas me risquer à goûter toutes les plantes que je rencontre.".

Instead of tying la liane to quelque chose:
	dire "La liane me permettrait plutôt d'attacher deux choses ensemble[_]!".
Instead of tying quelque chose to la liane:
	dire "La liane me permettrait plutôt d'attacher deux choses ensemble[_]!".

Part 11 - Forêt nord

La forêt-nord est au nord des ruines, à l'ouest de la source et au nord-ouest de la forêt-est. "La jungle, toujours aussi dense et impénétrable. Les arbres continuent au sud et au sud-est mais s'éclaircissent vers l'ouest, et le sol grimpe en direction du nord.[à la ligne]Un gargouillis se fait entendre à l'est, mais sinon, rien, pas le moindre bruit. C'est plutôt inquiétant…".
Le printed name est "Entre les arbres".
Le sound est "Pas un bruit…".
Le scent est "Tout possède une odeur riche, sauvage[_]: l'humus, les feuilles…".
La température est 4.

Chapter 1 - Branche

La branche est une chose dans la forêt-nord. "Une branche est tombée d'un arbre.".
La description est "Une lourde branche, quoique petite. Un coup de vent a dû la détacher.".
Le scent est "Elle a l'odeur de la forêt.".
La texture est "Elle possède la même texture que l'écorce qui recouvre les troncs.".
Le taste est "Ce n'est vraisemblablement pas comestible.".

Instead of tying la branche to quelque chose:
	dire "Je devrais attacher la branche à autre chose.".
Instead of tying quelque chose to la branche:
	dire "Je devrais attacher la branche à autre chose.".

Instead of tying la branche to le galet, fabriquer la pelle.
Instead of tying le galet to la branche, fabriquer la pelle.

To fabriquer la pelle:
	si le player porte la liane:
		retirer la liane du jeu;
		retirer la branche du jeu;
		retirer le galet du jeu;
		maintenant le player porte la pelle;
		dire "J'attache la branche au galet grâce à la liane afin de former une sorte de pelle.";
	sinon:
		dire "Je n'ai rien pour les attacher ensemble[_]!".
	
Chapter 2 - Pelle

La pelle est une chose.
La description est "Une pelle rudimentaire, faite d'un galet et d'une branche attachés ensemble par une liane. Je sais, on peut faire mieux comme outil…".

Comprendre "galet", "branche" et "liane" comme la pelle.

Part 12 - Montagne ouest

La montagne-ouest est au nord-est de la falaise-nord, au nord de la forêt-nord et au nord-ouest de la source. "Des contreforts d'où l'on a une magnifique vue sur l'ensemble de l'île. Impossible sinon de continuer vers le nord[_]: la pente est vraiment trop escarpée. La chaîne continue à l'est, je peux redescendre vers la forêt au sud et au sud-est, et vers la falaise au sud-ouest.".
Le printed name est "Au pied des montagnes".
Le sound est "Tout est plus calme ici.".
Le scent est "L'air est pur.".
La température est 4.

Instead of going bas quand la location est la montagne-ouest ou la location est la montagne-est:
	essayer de going south.

Instead of going nowhere quand la location est la montagne-ouest ou la location est la montagne-est:
	dire "Il serait trop dangereux de m'aventurer plus loin dans les montagnes."

Chapter 1 - Vue

La vue est une toile de fond hors de portée. La vue est dans la montagne-ouest et la montagne-est.
La description est "Les voyageurs et les explorateurs qui s'étaient aventurés par-delà la surface n'avaient pas exagéré dans leurs récits[_]: jamais je n'ai vu un paysage aussi beau. Sous l'eau, il n'y a pas ces forêts, ce ciel, cette lumière…[à la ligne]".
L' interaction lointaine est "Il s'agit d'un paysage, pas d'un objet.".

Comprendre "ile" ou "paysage" comme la vue.

Chapter 2 - Montagnes

Les montagnes sont des toiles de fond hors de portée. Les montagnes sont dans la source, la montagne-ouest et la montagne-est.
La description est "Des montagnes se dressent au nord, dans le lointain.".
L' interaction lointaine est "Il faudrait pour cela les gravir.".

Comprendre "montagne", "chaine" et "contrefort/contreforts" comme les montagnes.

Instead of examining le nord dans la source, essayer de examining les montagnes.

Chapter 3 - Silex

Les silex sont des choses dans la montagne-ouest.
La description est "Des pierres à feu, qui peuvent laisser échapper une étincelle si je les frotte.".
Le sound est "Je frappe les silex entre eux. Ils me renvoient, sans surprise, un bruit de caillou que l'on cogne.".
La texture est "Ils sont lisses, avec de nombreuses petites aspérités.".
Le taste est "Je pourrais me faire mal.".

Understand the command "entrechoquer" as "frotter".

Instead of rubbing les silex:
	si une chance de 1 sur 2 réussit et la location n'est pas le fond:
		dire "J'entrechoque les silex, et une étincelle jaillit";
		si la location est le souterrain et le souterrain est sombre:
			dire ". Par chance, elle tombe sur une sorte de liquide remplissant une cavité aménagée dans le sol[_]; il prend feu";
			maintenant le souterrain est éclairé;
		dire ".";
	sinon:
		dire "J'ai beau les entrechoquer, l'étincelle ne vient pas.".
	
Part 13 - Montagne est

La montagne-est est à l'est de la montagne-ouest, au nord-est de la forêt-nord et au nord de la source. "Les contreforts de la chaîne qui se dresse juste devant moi et qui continue à l'ouest. Ce serait pure folie de s'aventurer plus loin. Je devrais revenir vers la forêt au sud et au sud-ouest.".
Le printed name est "Au pied des montagnes".
Le sound est "Les montagnes sont plus calmes que la forêt.".
Le scent est "L'air est pur.".
La température est 4.

Part 14 - Source

La source est au nord de la forêt-est et au nord-est des ruines. "Un petit gargouillis sortant des rochers forme un petit lac. Il a l'air profond[à moins que le player porte l' orbe][_]; je ferais bien de me reposer un peu avant de continuer[fin à moins que]. D'ici, on peut voir les montagnes au nord, et les arbres tout autour, à l'ouest, au sud-ouest et au sud.".
Le printed name est "À la source".
Le sound est "[si incendie is happening]Le bois des arbres craque sous l'effet de la chaleur[sinon]Le son rassurant de l'eau qui se déverse dans le lac[fin si].".
Le scent est "L'odeur de la forêt est moins présente.".
La température est 0.

Instead of swimming quand la location est la source, essayer de going bas.

Chapter 1 - Lac

Le lac est une chose décorative dans la source.
Le printed name est "lac".
la description est "Un lac miroitant formé par la source sortant des rochers. Je devrais peut-être plonger.".
Le sound est "Le clapotis causé par la source se déversant dans le lac est apaisant.".
Le scent est "L'eau douce n'a pas la même odeur que l'eau salée. Je l'aime moins.".
La texture est "L'eau n'est pas très chaude.".
Le taste est "L'effet est bien différent de celui de l'eau salée.".

Comprendre "eau", "lac", "source", "gargouillis", "rochers" ou "rocher" comme le lac.

Instead of entering le lac, essayer de going bas.

Instead of taking le lac:
	si le player porte le galet ou le player porte la pelle, dire "J'ai de quoi transporter de l'eau, mais il serait difficile de me déplacer ensuite.";
	sinon dire "Je n'ai rien pour transporter de l'eau.".

Before eating le lac:
	dire "Cela ne se mange pas[_]!";
	arrêter l'action.

Instead of drinking le lac:
	dire "Je laisse un instant ma tête sous l'eau. J'ai très envie de plonger.".

Instead of inserting quelque chose into le lac:
	maintenant le noun est dans le fond;
	dire "Je jette [le noun] dans l'eau, et [il] [coules].".

Instead of throwing quelque chose at le lac:
	essayer de inserting le noun into le lac.

Instead of pushing ou pulling le lac:
	dire "Je ne peux pas faire cela[_]!".

Instead of searching le lac, dire "Je devrais plonger.".
Instead of looking under le lac, say "Cela me paraît difficile.".

Part 15 - Fond du lac

Le fond est en dessous de la source. "Sous l'eau, dans mon élément. [si la trappe est non-décrite]Mis à part les poissons et les algues, il n'y a que des rochers ici. Je devrais remonter[sinon]En plus des poissons, il y a une trappe qui était cachée par les algues[fin si].".
Le printed name est "Sous la surface de l'eau".
Le sound est "Le calme aquatique.".
Le scent est "Bien des odeurs me sont inconnues, néanmoins ces senteurs aquatiques sont réconfortantes.".
La température est 0.

Instead of exiting when la location est le fond, essayer de going haut.

Instead of doing something with la carte quand la location est le fond, dire "Mieux vaut garder ce morceau de parchemin là où il est. L'eau pourrait l'abîmer."

Chapter 1 - Poissons

Les poissons (m) sont des animaux décoratifs dans le fond.
La description est "De nombreux poissons nagent ici. Ils ne semblent pas se soucier des événements récents.".
Le sound est "Le bruit des poissons est bien assez audible pour le chasseur expérimenté.".
Le scent est "L'odeur du poisson, qui me rappelle les jours de chasse, sous l'océan.".

Instead of touching les poissons, dire "Les poissons s'échappent avant que je puisse les toucher.".

Comprendre "poisson" comme les poissons.

Instead of taking ou attacking les poissons, dire "Je suis agile à la chasse, mais je dois me concentrer sur autre chose en ce moment.".

Instead of tasting les poissons, essayer de taking les poissons.
Instead of searching les poissons, essayer de taking les poissons.
Before eating les poissons:
	essayer de taking les poissons;
	stop the action.

Chapter 2 - Rochers

Les rochers sont des choses décoratives dans le fond.
La description est "Les rares endroits n'ayant pas été colonisés par les algues sont occupés par des rochers.".
La texture est "Ils possèdent de nombreuses aspérités.".
Le taste est "Pas très goûteux.".

Comprendre "rocher" ou "fond" comme les rochers.

Chapter 3 - Algues

Les algues (f) sont des choses décoratives dans le fond.
La description est "De longues algues poussant sur le fond du lac[si la trappe est non-décrite]. Bizarrement, il y en a plus à un endroit[fin si].".
Le scent est "Je n'arrive pas à identifier cette espèce.".
La texture est "Ces algues ne sont pas bien différentes de celles que l'on trouve sous l'océan.".

Comprendre "algue" comme les algues.

Instead of taking ou pulling les algues:
	si la trappe est non-décrite, essayer de searching les algues;
	sinon dire "Je n'ai pas le temps de les arracher.".

Instead of searching les algues:
	si la trappe est non-décrite:
		dire "Là[_]! il y a une trappe cachée sous les algues[_]!";
		établir les pronoms pour la trappe;
		maintenant la trappe est décrite;
	sinon:
		dire "J'ai déjà suffisamment fouillé les algues.".

Before eating les algues:
	dire "Je ne connais pas cette espèce et je ne sais pas si elle est comestible.";
	arrêter l'action.

Instead of tasting les algues, essayer de eating les algues.

Chapter 4 - Trappe

La trappe est une porte fermée verrouillée verrouillable non-décrite. La trappe est à l'intérieur du fond et à l'extérieur du souterrain.
La description est "Une trappe qui a gardé sa solidité malgré sa pourriture. Elle possède une serrure.".
Le sound est "Elle grince un peu.".
Le scent est "Elle dégage l'odeur du bois pourrissant.".
La texture est "Elle semble fragile, mais elle ne l'est finalement pas tant que ça.".
Le taste est "Le bois pourri n'a pas un bon goût.".

Instead of doing something with la trappe non-décrite, dire "Je ne vois rien de tel, à moins que cela soit sans grande importance.".

Instead of attacking la trappe, dire "Mes efforts sont vains[_]: elle est trop solide.".

Instead of going bas quand la trappe est décrite et la location est le fond, essayer de going inside.

After going to le souterrain:
	maintenant la trappe est non-décrite;
	continue the action.
Before going from le souterrain to le fond, maintenant la trappe est décrite.

Part 16 - Souterrain

Le souterrain est un endroit sombre.
La description est "Une galerie fraîche et humide, éclairée faiblement par le feu que j'ai allumé. Elle semble artificielle et d'antiques étais la soutiennent encore du mieux qu'ils peuvent. Le souterrain se poursuit vers l'ouest. À moins que j'aie besoin de sortir.".
Le printed name est "Le long d'un souterrain".
Le sound est "Silence total.".
Le scent est "L'endroit est très humide.".
La température est 2.

Rule for printing the description of a dark room when la location est le souterrain:
	dire "Après quelques secondes passées à nager, j'ai émergé dans cet endroit sombre. Impossible de continuer sans lumière.".

Chapter 1 - Étais

Les étais sont des choses décoratives dans le souterrain.
La description est "Ils sont rongés par l'humidité. Qu'ils soient encore debout est un miracle.".
Le scent est "De la pourriture flotte dans l'air. J'espère que les étais sont assez solide.".
La texture est "Ils paraissent frêles sous la masse écrasante du plafond.".
Le taste est "Ils sont déjà si humides. Ça les fragiliserait peut-être encore plus[_]?".

Comprendre "etais" ou "etai" comme les étais.

Instead of attacking les étais:
	dire "Afin de vérifier la solidité des étais, je donne un coup à celui le plus proche. Il se brise, et la galerie s'effondre sur moi.";
	attendre une touche;
	fin de l'histoire en disant "C'est la fin[_]!".

Instead of pushing les étais, essayer de attacking les étais.
Instead of pulling les étais, essayer de attacking les étais.

Chapter 2 - Liquide

Le liquide est une chose décorative dans le souterrain.
La description est "Ce liquide, remplissant un trou aménagé au sol, s'est enflammé au contact de l'étincelle. Il n'éclaire pas beaucoup, et ne dégage pas trop de chaleur.".
Le scent est "Le liquide ne dégage aucune odeur.".
La texture est "Il n'y a pas de feu chez nous, mais les récits nous ont bien gardés de nous en approcher.".
Le taste est "Je n'ai pas envie d'essayer.".

Comprendre "feu/flammes/flamme" comme le liquide.

Instead of taking le liquide, try touching le liquide.

Instead of drinking le liquide, essayer de tasting le liquide.

Instead of inserting quelque chose into le liquide, dire "Au risque de [à propos du noun][le] perdre pour toujours[_]?".

Part 17 - Sanctuaire

Le sanctuaire est à l'ouest du souterrain. "Un immense dôme de pierre surplombe cette immense cavité rocheuse. Au centre, sur un magnifique piédestal ouvragé et doré à l'or fin, je l'ai enfin trouvé…".
Le printed name est "Dans le sanctuaire".
Le sound est "Un silence absolu.".
Le scent est "L'air est étonnament assez pur.".
La température est 1.

Le souterrain est à l'extérieur du sanctuaire.
Nulle part est à l'intérieur du souterrain.

Chapter 1 - Autel

L' Autel de la Lumière (m) est un support. "L'Autel de la Lumière est là, devant moi." L' autel est dans le sanctuaire. 
L' indefinite article de l' autel est "l[']".
La description est "L'Autel de la Lumière. Il n'y a pas d'autres mots pour le décrire. Il est magnifique."
Le scent est "Du respect s'impose.".
La texture est "Je n'ose pas le toucher tant il est beau.".
Le taste est "Un peu de respect, voyons[_]!".

Comprendre "piedestal" ou "lumiere" comme l' autel.

After examining l' Autel pour la première fois:
	dire "Soudain, l'air au-dessus de l'Autel se trouble.";
	attendre une touche;
	dire "Un spectre apparaît alors. Un spectre de Nâga[_]! Celui-ci prend la parole avant que je puisse réagir[_]:[saut de paragraphe]";
	attendre une touche;
	dire "«[_]Bienvenue en ces lieux. Tu dois être celui dont parle la prophétie. Vois-tu, c'est moi qui l'ai annoncée, alors que notre monde s'écroulait.[saut de paragraphe]";
	attendre une touche;
	dire "«[_]L'Antique Démon de la Terre était en train de se réveiller dans une rage destructrice, après cent mille ans de sommeil. Mon peuple pria alors la Déesse du Ciel, lui demandant de vaincre le Démon.[saut de paragraphe]";
	attendre une touche;
	dire "«[_]Elle entendit nos prières, et le combat fut titanesque. Après des années de lutte, notre Déesse gagna enfin. Mais les conséquences furent terribles…[_]» Le fantôme marque une pause, perdu dans ses pensées.[saut de paragraphe]";
	attendre une touche;
	dire "«[_]Les continents avaient tous sombré dans les abysses, et nous fûmes obligés de quitter définitivement la surface… Oui, tu as compris, mon peuple et le tien ne font qu'un. Tu es ici dans le premier temple nâga.[saut de paragraphe]";
	attendre une touche;
	dire "«[_]Mais avant notre départ, la Déesse me donna l'Orbe Étincelant, seul objet renfermant le pouvoir de vaincre le Démon, quand il se réveillerait de nouveau après cent mille autres années de torpeur.[saut de paragraphe]";
	attendre une touche;
	dire "«[_]Cette histoire est passée dans le mythe[_]; seule la prophétie fut transmise à chaque génération. Et maintenant, elle se réalise. Va, Sauveur du monde[_]! Je te remets l'Orbe[_]![_]»[saut de paragraphe]";
	attendre une touche;
	dire "Soudain la terre tremble, au moment même où le spectre disparaît. Vite, je dois retourner parmi les miens[_]!";
	maintenant le player porte l' orbe.
	
Chapter 2 - Orbe

Orbe (m) est une chose.
Le printed name est "l'Orbe Étincelant".
L' indefinite article est "l[']".
La description est "L'Orbe Étincelant, seule arme capable de vaincre le Démon de la Terre. Il brille de mille éclats et dégage une douce chaleur.".
Le sound est "Quelle idée étrange.".
Le scent est "Ce serait manquer de respect.".
La texture est "Je préfère la manipuler le moins possible et la garder précieusement.".
Le taste est "Ça ne se fait pas, quand même[_]!".

Comprendre "etincelant" comme l' orbe.

Instead of dropping l' orbe, dire "Je dois le garder à tout prix[_]!".
Instead of inserting l' orbe into quelque chose, essayer de dropping l' orbe.
Instead of putting l' orbe on quelque chose, essayer de dropping l' orbe.

Part 18 - Incendie

La jungle est une région. La forêt-est, la forêt-nord et les ruines sont dans la jungle.
	
L' incendie est une scène. l' incendie begins when le player porte l' orbe et la location est la source.

When l' incendie begins:
	dire "Le volcan de l'île est entré en éruption et un incendie a gagné la forêt[_]! Je dois vite rejoindre l'océan[_]!".
	
Every turn during l' incendie:
	si la location n'est pas l' océan et la location n'est pas le fond et la location n'est pas le souterrain et la location n'est pas le sanctuaire:
		dire "[parmi]Vite[ou]Quelle chaleur[ou]Je dois rejoindre l'océan[au hasard][_]!";
	si la température de la location n'est pas 0, diminuer l' hydratation du player par 5.

Instead of going to la jungle during l' incendie, dire "Un incendie s'est déclaré dans toute la forêt[_]! Je dois trouver un autre passage[_]!".

Volume 4 - Méta

Book 1 - Données bibliographiques

Le story headline est "Une exploration interactive".
Le story genre est "Fantasy".
Le release number est 4.
La story description est "Après cent mille ans de sommeil, le Démon de la Terre s'est éveillé. Afin de sauver le monde du chaos, moi, membre du peuple Nâga choisi par la prophétie, dois me rendre à la surface et trouver, au cœur d'une île déserte, l'Autel de la Lumière, où la solution sera dévoilée…".
La story creation year est 2011.

Release along with cover art[, a solution, the source text and a file of "Changelog" called "LTN-changelog.txt"].

Book 2 - Commandes d'information

Part 1 - Aide 

Understand "aide", "infos", "info", "informations" or "information" as a mistake ("Ce jeu est une fiction interactive[_]; il se joue en tapant des commandes à l'infinitif telles que «[_]voir[_]», «[_]prendre [italique]objet[romain][_]», etc. (pour plus d'informations, consultez <http://fiction-interactive.fr/>).[saut de paragraphe]Dans cette fiction interactive en particulier, l'hydratation du héros (notée en haut à droite) correspond à l'eau qu'il reste à son corps. Sa valeur maximale est 100[_]%.[à la ligne]Chaque tour, elle baisse en fonction de la chaleur de l'endroit. La couleur dans laquelle est écrit le nom du lieu permet de s'en faire une idée[_]: [en rouge]rouge[romain] pour un lieu chaud, [en jaune]jaune[romain] pour un lieu à température moyenne et [en vert]vert[romain] pour un lieu frais.[à la ligne]Le [en bleu]bleu[romain] indique quant à lui que l'endroit contient de l'eau[_]; l'hydratation remontera alors à 100[_]%. Le héros meurt si elle atteint 0.[saut de paragraphe]Si, pour une raison ou une autre, vous n'êtes pas en mesure de voir les couleurs, la commande «[_]couleurs arrêt[_]» fera en sorte que la température d'un endroit sera plutôt indiquée entre parenthèses après son nom (tapez «[_]couleurs marche[_]» pour que les couleurs soient de nouveau utilisées).[saut de paragraphe]Tapez «[_]licence[_]» pour l'obtenir et «[_]auteur[_]» pour en savoir plus sur l'auteur.").

Part 2 - Auteur

Understand "credits", "credit", "auteur" or "auteurs" as a mistake ("C'est en fouillant des ruines anciennes que l'auteur, Nathanaël Marion, a découvert d'antiques récits relatant les aventures du Nâga qui sauva le monde jadis. Il a cru bon de les partager, et c'est ainsi qu'est né ce jeu. Il est cependant difficile de comprendre toutes les subtilités de cette langue disparue, c'est pourquoi de l'aide pour déchiffrer ces écrits est disponible sur son site, <http://ulukos.com>. De plus, il est possible de le contacter à <natrium729@gmail.com>. Il remercie les personnes qui l'ont aidé à transcrire le texte, notamment les membres de [italique]Fiction-interactive.fr[romain] (et en particulier Otto Grimwald, Stormi, [gras]TODO: préférez-vous que je mette vos vrais noms[_]? Aussi, ajouter les personnes qui manquent[romain]).[à la ligne]Nathanaël Marion poursuit en ce moment ses recherches, car l'aventure continue dans [italique]Entre Terre et Ciel[romain][_]!").

Part 3 - Licence

Understand "licence" or "license" as a mistake ("©[_]2015[_]Nathanaël[_]Marion[saut de paragraphe][gras]Pour faire bref[_]:[à la ligne][romain]Ce programme est un logiciel libre[_]; vous pouvez le redistribuer ou le modifier suivant les termes de la «[_]GNU General Public License[_]» telle que publiée par la Free Software Foundation[_]: soit la version[_]3 de cette licence, soit toute version ultérieure.[saut de paragraphe]Ce programme est distribué dans l'espoir qu'il vous sera utile, mais SANS AUCUNE GARANTIE[_]: sans même la garantie implicite de COMMERCIALISABILITÉ ni d'ADÉQUATION À UN OBJECTIF PARTICULIER. Consultez la Licence Générale Publique GNU pour plus de détails.[saut de paragraphe][gras]Pour faire long, [romain]consultez[_]: <http://www.gnu.org/licenses>.").