# Le Temple nâga #

*Après cent mille ans de sommeil, le Démon de la Terre s'est éveillé. Afin de sauver le monde du chaos, moi, membre du peuple Nâga choisi par la prophétie, dois me rendre à la surface et trouver, au cœur d'une île déserte, l'Autel de la Lumière, où la solution sera dévoilée…*

## Jouer ##

*Le Temple nâga* est une fiction interactive, premier (et court) volet d'une trilogie. Elle se joue en tapant des verbes à l'infinitif afin de guider le héros dans sa quête.

Si vous voulez essayer, ce n'est pas ici qu'il faut regarder ! Vous pouvez jouer en ligne à une vieille version [à cet endroit](http://ifiction.free.fr/index.php?id=jeu&j=054), et la dernière version, qui a été beaucoup améliorée entre temps, est disponible [ici](http://ulukos.com/wp-content/uploads/2015/05/le-temple-naga.gblorb).

## La Source ##

La source de ce jeu est visualisable dans ce dépôt (dans le fichier « TempleNaga.inform/Source/story.ni ») afin de satisfaire votre curiosité. Elle est compilable avec la version 6L38 d'Inform 7.

Le répertoire en « -6G » contient la source de l'une des premières versions du *Temple nâga,* écrite avec la version 6G60 d'Inform 7, qui est gardée à titre historique.

## Pour m'aider ##

Rien de plus simple : jouez ! Cela me ferait également plaisir si vous me faites part de vos commentaires/impressions/remarques. Vous pouvez me contacter via [mon site](http://ulukos.com/contact/) ou sur [le forum de la communauté francophone dédié aux fictions interactives](http://ifiction.free.fr/taverne/index.php).

Si vous trouvez une erreur dans le jeu, vous pouvez utiliser les mêmes liens que ci-dessus, ou bien l'outil de signalement de bug (menu ci-contre).